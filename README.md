# Php-Sdk (Appivo)

Connect your Appivo app with any application or website using php

## Overview
- Simple integration of existing applications with appivo backend
- Simple authentication using key, secret provided on client construction
- Abstracts away underlying communication complexities

## Requirements
    php: >=5.5;

## Api Methods:

### 1.  appivo_client($key, $secret, $host)

Constructor to create an appivo client

#### Required:
$key (string), $secret (string) - The application user's key and secret. This can be generated from appivo platform under user's settings

#### Optional:
$host (string)  - The host address of appivo application.
Defaults to: https://apps.appivo.com/

#### Return: 
Object of appivo_client that can be used to call appivo_client class methods

### 2. getAllRecords($app, $model)

Fetches all records of a model

#### Required:
$app (string) - Name of the appivo application
$model (string)- Name of model from which data is to be retrieved

#### Return: 
Array of records

### 3. getRecord($app, $model, $id)

Fetches record of the given id

#### Required:
$app (string) - Name of the appivo application
$model (string) - Name of model from which data is to be retrieved
$id (string) - id of the record to be fetched

#### Return: 
Array containing one record

### 4. createRecord($app, $model, $record)

Creates a new record of the model

#### Required:
$app (string) - Name of the appivo application
$model (string) - Name of model from which data is to be retrieved
$record (array) - Associative array comprising of key-value pair of properties that make up model record

#### Return:
Array containing newly created record

### 5. updateRecord($app, $model, $record)

Updates the record 

#### Required:
$app (string) - Name of the appivo application
$model (string) - Name of model from which data is to be retrieved
$record (array) - Associative array with updated value of record.

#### Return:
Array containing single updated record

### 6. deleteRecord($app, $model, $id)

Deletes the record with given id

#### Required:
$app (string) - Name of the appivo application
$model (string) - Name of model from which data is to be retrieved
$id (string) - id of the record to be deleted

#### Return: 
True on successful deletion of record

### 7. executeQuery($app, $queryName, $paramsArray)

Executes given query with parameters

####  Required:
$app (string) - Name of the appivo application
$queryName (string) - Name of query to be executed

#### Optional:
$paramsArray (array) - Associative array comprising of key-value pair of parameters to be supplied to the executing query

#### Return: 
Array of resulting records

## Sample Usage:

### 1. new appivo_client($key, $secret, $host)

```php
$key = 'xxxxxxxxxxxxxxxxxxxxxx';
$secret = 'xxxxxxxxxxxxxxxxxxxxxxxx';
$appivo_client = new appivo_client($key, $secret); 
$appivo_client = new appivo_client($key, $secret, 'qa.appivo.com'); //With optional host
```
### 2. getAllRecords($app, $model)
```php
$records = $appivo_client->getAllRecords('TestApp','Bike');
```
### 3. getRecord($app, $model, $id)
```php
$record = $appivo_client->getRecord('TestApp','Bike', '1662026b66306e5ab9ae955700000001');
```
### 4. createRecord($app, $model, $record)
```php
$record = array("BikeName" => "Royal Enfield","Power" => "210CC");
$newRecord = $appivo_client->createRecord('TestApp','Bike', $record);
```
### 5. updateRecord($app, $model, $record)
```php
$record = array("id" => "1662af3140b0f269c75a097200000001","BikeName" => "Bajaj","Power" => "900CC", "ver"=>"0");
$updatedRecord = $appivo_client->updateRecord('TestApp','Bike', $record);
```
### 6. deleteRecord($app, $model, $id)
```php
$success = $appivo_client->deleteRecord('TestApp','Bike', "1662af3140b0f269c75a097200000001");
if ($success) {
    // Logic on deletion
}
```
### 7. executeQuery($app, $queryName, $paramsArray)

 Query with parameters:
```php
$params = array("BikeName" => "Royal Enfield", "BikePower" => "210CC");
$records = $appivo_client->executeQuery('TestApp','BikesByNamePower', $params);
```
Query without parameters:
```php
$records = $appivo_client->executeQuery('TestParv','GetAllBikes');
```
## License
----
