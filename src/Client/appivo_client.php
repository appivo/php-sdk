<?php
namespace Appivo\Client;

use GuzzleHttp\Client as GuzzleClientForAppivo;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class appivo_client
{

    private $_accessToken;
    private $_baseUri;
    private $_host;
    public $header_signature = "X-Signature";

    public function __construct($accessToken, $host = 'apps.appivo.com')
    {
        if (! (isset($accessToken) && strlen($accessToken) > 0)) {
            throw new \InvalidArgumentException('accessToken is required');
        }
 
        try {
            $this->_accessToken = $accessToken;
            $this->_host = $host;
            $this->_baseUri = 'https://' . $this->_host . '/api/app/';
            $this->client = new GuzzleClientForAppivo();
        }
        catch (Exception $ex) {
            echo 'Failed to create client with following error: '.$ex;
            return null;
        }
        
    }

    private function callApi($finalUrl, $method, $record = array(), $customHeadersArray = array())
    {

        if (! (isset($finalUrl) && strlen($finalUrl) > 0)) {
            throw new \InvalidArgumentException('Request Uri not supplied');
        }
        if (! (isset($method) && strlen($method) > 0)) {
            throw new \InvalidArgumentException('Method type not supplied');
        }

        try {
            $currentClient = $this->client;
            $response;

            if (count($record) > 0)
            {
                $paramsValue = json_encode($record);
            }
            else
            {
                $paramsValue = null;
            }

            $headerArray = array(
                'Authorization' => $this->_accessToken,
                'Content-Type' => 'application/json'
            );

            if (count($customHeadersArray) > 0) {
                foreach ($customHeadersArray as $key => $value)
                {
                    $headerArray[$key] = $value;
                }
            }

            $response = $currentClient->request($method, $finalUrl, ['headers' => $headerArray, 'body' => $paramsValue]);

            $responseStatusCode = $response->getStatusCode();

            if ($method == 'DELETE')
            {
                if ($response->getStatusCode() === 200)
                {
                    return true;
                }
            }

            return json_decode($response->getBody());
        }
        catch (ClientException $ex) {

            if ($ex->getCode() == 403) {
                throw new \Exception("Invalid key/secret. Details:".$ex->getResponse()->getBody()->getContents());
            }

            if ($ex->getCode() == 404) {
                throw new \Exception("Resource requested does not exist. Details:".$ex->getResponse()->getBody()->getContents());
            }
        }
        catch (ServerException $ex) {
            throw new \Exception("Server Error Occured. Details:".$ex->getResponse()->getBody()->getContents());
        }
        //Other Exceptions
        catch (\Exception $ex) {
            throw $ex;
        }

    }

    public function getAllRecords($app, $model)
    {
        if (! (isset($app) && strlen($app) > 0)) {
            throw new \InvalidArgumentException('App-Name Required');
        }
        if (! (isset($model) && strlen($model) > 0)) {
            throw new \InvalidArgumentException('Model-Name Required');
        }

        try {
            $finalUrl = $this->_baseUri . $app . '/model/' . $model . '/record/';
            return self::callApi($finalUrl, 'GET');
        }
        catch (\Exception $ex) {
            throw $ex;
        }
        
    }

    public function getRecord($app, $model, $id, $customHeaders = array())
    {
        if (! (isset($app) && strlen($app) > 0)) {
            throw new \InvalidArgumentException('App-Name Required');
        }
        if (! (isset($model) && strlen($model) > 0)) {
            throw new \InvalidArgumentException('Model-Name Required');
        }

        try {
            $finalUrl = $this->_baseUri . $app . '/model/' . $model . '/record/' . $id;

            if ((isset($customHeaders) && count($customHeaders) > 0)) {
                return self::callApi($finalUrl, 'GET', array(), $customHeaders);
            } else {
                return self::callApi($finalUrl, 'GET');
            }

        }
        catch (\Exception $ex) {
            throw $ex;
        }
        
    }

    //record has to be an associative array constituting of form parameters
    public function createRecord($app, $model, $record)
    {
        if (! (isset($app) && strlen($app) > 0)) {
            throw new \InvalidArgumentException('App-Name Required');
        }
        if (! (isset($model) && strlen($model) > 0)) {
            throw new \InvalidArgumentException('Model-Name Required');
        }

        try {
            $finalUrl = $this->_baseUri . $app . '/model/' . $model . '/record/';
            return self::callApi($finalUrl, 'POST', $record);
        }
        catch (\Exception $ex) {
            throw $ex;
        }
    }

    //record has to be an associative array constituting of form parameters
    public function updateRecord($app, $model, $record)
    {
        if (! (isset($app) && strlen($app) > 0)) {
            throw new \InvalidArgumentException('App-Name Required');
        }
        if (! (isset($model) && strlen($model) > 0)) {
            throw new \InvalidArgumentException('Model-Name Required');
        }
        if (! (isset($record))) {
            throw new \InvalidArgumentException('Record to update Required');
        }
        

        try {
            $finalUrl = $this->_baseUri . $app . '/model/' . $model . '/record/' . $record['id'];
            return self::callApi($finalUrl, 'PUT', $record);
        }
        catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function deleteRecord($app, $model, $id)
    {
        if (! (isset($app) && strlen($app) > 0)) {
            throw new \InvalidArgumentException('App-Name Required');
        }
        if (! (isset($model) && strlen($model) > 0)) {
            throw new \InvalidArgumentException('Model-Name Required');
        }
        if (! (isset($id) && strlen($id) > 0)) {
            throw new \InvalidArgumentException('Record id to delete Required');
        }

        try {
            $finalUrl = $this->_baseUri . $app . '/model/' . $model . '/record/' . $id;
            return self::callApi($finalUrl, 'DELETE');
        }
        catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function executeQuery($app, $queryName, $paramsArray = array())
    {
        if (! (isset($app) && strlen($app) > 0)) {
            throw new \InvalidArgumentException('App-Name Required');
        }
        if (! (isset($queryName) && strlen($queryName) > 0)) {
            throw new \InvalidArgumentException('Query-Name Required');
        }


        try {
            $finalUrl = $this->_baseUri . $app . '/query/' . $queryName . '/result/';
            $modifiedArray = array();
            if (count($paramsArray) > 0)
            {

                foreach ($paramsArray as $key => $value)
                {
                    $newKey = '$' . $key;
                    $modifiedArray[$newKey] = $value;
                }

                $queryData = http_build_query($modifiedArray);
                $finalUrl = $finalUrl . '?' . $queryData;
            }

            return self::callApi($finalUrl, 'GET');
        }
        catch (\Exception $ex) {
            throw $ex;
        }
    }


    public function invokeService($app, $restrulename, $method, $postparams = array()) {
        if (! (isset($restrulename) && strlen($restrulename) > 0)) {
            throw new \InvalidArgumentException('Name of Rest Rule to invoke is required');
        }
        if (! (isset($method) && strlen($method) > 0)) {
            throw new \InvalidArgumentException('Method name Required');
        }

        $finalUrl = $this->_baseUri . $app . '/service/' .$restrulename;

        return self::callApi($finalUrl, $method, $postparams);

    }

    public function createTenant($tenant) {

        if (! isset($tenant)) {
            throw new \InvalidArgumentException('Tenant Name Required');
        }

        if (! isset($tenant["tenant"])) {
            throw new \InvalidArgumentException('Tenant Information Required');
        }

        if (! isset($tenant["tenantDetails"])) {
            throw new \InvalidArgumentException('Tenant Details Required');
        }

        if (! (isset($tenant["subscription"]))) {
            throw new \InvalidArgumentException('Subscription Details Required');
        }

        try {
            $finalUrl = 'https://'.$this->_host.'/api/signup?createTenant=true';
            return self::callApi($finalUrl, 'POST', $tenant);
        }
        catch (\Exception $ex) {
            throw $ex;
        }
    }

}
?>